import { Component, OnInit } from '@angular/core';
import {ServicesService } from '../services/services.service';
import {LoadingService } from '../services/loading.service';
import { AlertControllerService } from '../services/alert-controller.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user:any= {};
  userdata: any;
  public loginForm: FormGroup;
  userdetail:any;
 
  constructor(public svs: ServicesService, public formBuilder: FormBuilder, public loadingCtrl: LoadingService,
     public alertCtrl: AlertControllerService, public router: Router, public storage: Storage, private authService: AuthenticationService) {
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['',Validators.compose([Validators.required])],
      isremember: [false]
  });
  this.user.isremember = false;
  }

  ngOnInit(){
    this.checkCredential();

  }
  

  async loginU(): Promise<any>{
    console.log(this.user.isremember);
    if(this.loginForm.valid)
    {
      let email = this.user.email;
      let password = this.user.password;
     
      var isRemember = {
        email: email,
        password: password
      }
      await this.loadingCtrl.present();
      this.authService.loginUser(email, password).subscribe(async (res) => {
       console.log(res);
        if(res){
          console.log('success');
          this.userdata = res;                    
         let response = await this.storage.set('access_token', this.userdata.access_token);
           if(response){
            if(this.user.isremember === true){
              this.storage.set('isRemember', isRemember);
             }else{
              this.storage.remove('isRemember');
             }
          //    this.storage.get('access_token').then((val) => {
          //     alert('dd'+ val);
          //  });
            this.loadingCtrl.dismiss();
            this.authService.authState.next(true);
           // this.getUserDetail();
           }
        
          
        }else{
          this.alertCtrl.presentAlert('Alert', 'Invalid login');
        }
                
        }, err => {
          this.alertCtrl.presentAlert('Alert', 'Wrong credientials, please try again');
          this.loadingCtrl.dismiss();
        })
    }
    else{
      this.alertCtrl.presentAlert('Required', 'All Fields are required');
    }
    
  }
  checkCredential(){
    this.storage.get('isRemember').then((val) => {
          if(val){
            this.user.email = val.email;
            this.user.password = val.password;
            this.user.isremember = true;
            console.log(this.user.email);
          }else{
            this.user.isremember = false;
            console.log("not exists");
          }
    });
  }

  register(){
    this.router.navigate(['signup']);
  }

    getUserDetail(){
    this.svs.getUserDetail().subscribe(async (res) => {
      if(res){
        this.userdetail = res;
        this.svs.userdetail = this.userdetail;
        console.log('uds' + JSON.stringify(this.svs.userdetail));
        this.router.navigate(['/home']);
      }
      }, err => {
        console.log(err);
      })
  }

  forgotpass(){
    this.router.navigate(['/forgotpass']);
  }

  resetpass(){
    let data = {email:'vicky@gmail.com',token:'1234'}
    this.router.navigate(['/resetpassword',data]);
  }

}