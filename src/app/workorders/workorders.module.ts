import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkordersRoutingModule } from './workorders-routing.module';
import { ListComponent } from './list/list.component';
import { NewComponent } from './new/new.component';
import { ViewComponent } from './view/view.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiFileUploadComponent } from '../components/multi-file-upload/multi-file-upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [ListComponent, NewComponent, ViewComponent, MultiFileUploadComponent],
  imports: [
    CommonModule,
    WorkordersRoutingModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    NgxIonicImageViewerModule,
    ComponentsModule
  ]
})
export class WorkordersModule { }
