import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ViewComponent } from './view/view.component';
import { NewComponent } from './new/new.component';


const routes: Routes = [
  {
    path:'',
    redirectTo:'list',
    pathMatch:'full'
  },
  {
    path:'list/:type/:id/:status',
    component:ListComponent
  },
  {
    path:'view/:id',
    component:ViewComponent
  },
  {
    path:'new',
    component:NewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkordersRoutingModule { }
