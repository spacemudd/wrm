import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {ServicesService } from '../../services/services.service';
import {LoadingService } from '../../services/loading.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
workdata:any = [];
type:any;
id:any = 0 ;
status:any;

  constructor(public router: Router, public svs: ServicesService, public loadingCtrl: LoadingService, public activatedRoute: ActivatedRoute, public navCtrl: NavController) {
    this.type = this.activatedRoute.snapshot.paramMap.get('type');
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.status = this.activatedRoute.snapshot.paramMap.get('status');
   }

  ngOnInit() {
    this.getWorkorders();
  }

  getWorkorders(){
    this.loadingCtrl.present();
    this.svs.getWorkorders(this.type, this.id, this.status).subscribe(async (res) => {
      this.workdata = res;
      console.log(res);
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  addWorkOrder(){
    this.router.navigate(['workorders/new']);
  }

  viewWorkOrder(id:any){
    this.router.navigate(['workorders/view/' + id]);
  }

}