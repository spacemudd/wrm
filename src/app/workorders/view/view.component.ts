import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import {ServicesService } from '../../services/services.service';
import {LoadingService } from '../../services/loading.service';
import { AlertControllerService } from '../../services/alert-controller.service';
import { ModalController, NavController } from '@ionic/angular';
import { UserNoteComponent } from '../../components/user-note/user-note.component';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit {
  Id:any = 0;
  workorders:any= {};
  branch:any = {};
  priority:any = {};
  aimmable:any = {};
  userPermission: Array<string>;
  can_be_returned_to_branch:boolean;
  is_returned:boolean;
  can_be_escalated_by_user:boolean;
  comment:any;
  notes:any = {'note':''};
  comments:any;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, public loadingCtrl: LoadingService, public svs: ServicesService, public alertCtrl: AlertControllerService, public modalController: ModalController, public navCtrl: NavController, public actionSheetController: ActionSheetController) { 
    this.Id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.userPermission = this.svs.userdetail.permissions;
    this.getWorkordersById();
  }

  async getWorkordersById(){
   await this.loadingCtrl.present();
    this.svs.getWorkordersById(this.Id).subscribe(async (res) => {
      this.workorders = res;
      this.branch = this.workorders.created_by_branch;
      this.priority = this.workorders.priority;
      this.aimmable = this.workorders.aimmable;
      this.comments = this.workorders.comments;
      this.can_be_returned_to_branch = this.workorders.can_be_returned_to_branch;
      this.is_returned = this.workorders.is_returned;
      this.can_be_escalated_by_user = this.workorders.can_be_escalated_by_user;
      console.log("comments - " + JSON.stringify(this.comments));
      console.log(this.workorders);
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  // completeWork(){
  //   this.loadingCtrl.present();
  //   this.svs.completeWork(this.Id).subscribe(async (res) => {
  //     console.log(res);
  //     if(res){
  //       this.alertCtrl.presentAlert('Success', 'Work conpleted successfully');
  //     }
  //     this.loadingCtrl.dismiss();
  //     }, err => {
  //       console.log(err);
  //       this.loadingCtrl.dismiss();
  //     })
  // }

  // escalateWork(){
  //   this.loadingCtrl.present();
  //   this.svs.escalateWork(this.Id).subscribe(async (res) => {
  //     console.log(res);
  //     this.alertCtrl.presentAlert('Success', 'Work escalated successfully');
  //     this.loadingCtrl.dismiss();
  //     }, err => {
  //       console.log(err);
  //       this.loadingCtrl.dismiss();
  //     })
  // }

  addWorkOrder(){
     this.router.navigate(['workorders/new']);
  }

  async openModal(val:any){
    const modal = await this.modalController.create({
      component:UserNoteComponent,
      componentProps:{id: this.Id, value: val},
      cssClass:'modalCss'
    });
    return await modal.present();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      //header: 'Actions',
      animated: true,
      buttons: this.createButtons(),
      cssClass:'custom-sheet'
    });
    await actionSheet.present();
    
  }

  createButtons() {
    let buttons = [];
    let cancel_button = {
      text:'الإجاراءات',
      icon: 'close',
      role:'destructive',
      handler:() => {
       console.log('close');
      }
    }
    buttons.push(cancel_button);

    if(this.userPermission.includes('can-escalate') && this.can_be_escalated_by_user == true){
      let button_escalate = {
        text: 'تصعيد',
        //role: 'destructive',
        handler: () => {
          this.openModal('escalate');
        }
      }
      buttons.push(button_escalate);
    }
    
    if(this.workorders.can_be_completed_by_user == true){
      let button_complete = {
        text: 'تم',
        //role: 'destructive',
        handler:() => {
          this.openModal('complete');
        }
      }
      buttons.push(button_complete);
    }

    if(this.userPermission.includes('return-work-orders') && this.can_be_returned_to_branch == true){
      let button_return = {
        //text:'رجاع الطلب',
        text:'إرجاع الطلب',
        //role: 'destructive', 
        handler:() => {
          this.openModal('return');
        }
      }
      buttons.push(button_return);
    }

    if(this.userPermission.includes('respond-to-returned-work-order') && this.is_returned == true){
      let button_respond = {
        text:'ضف رد على إرجاع الطلب',
        //role: 'destructive',
        handler:() => {
          this.openModal('respond');
        }
      }
      buttons.push(button_respond);
    }

    

    return buttons;
  }

  addComment(){
    if(!this.comment){
      this.alertCtrl.presentAlert('Required', 'Please add your comment');
    }else{
      this.notes.note = this.comment;
      this.loadingCtrl.present();
      this.svs.addWorkOrderComment(this.Id, this.notes).subscribe(async (res) => {
          console.log(res);
          this.loadingCtrl.dismiss();
          this.alertCtrl.presentAlert('Success', 'Comment added successfully');
          this.getWorkordersById();
          }, err => {
          console.log(err);
          this.loadingCtrl.dismiss();
        })
    }
  }


}