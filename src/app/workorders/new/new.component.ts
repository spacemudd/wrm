import { Component, OnInit, ViewChild } from '@angular/core';
import { MultiFileUploadComponent } from '../../components/multi-file-upload/multi-file-upload.component';
import { Router } from '@angular/router';
import { AlertControllerService } from '../../services/alert-controller.service';
import {ServicesService } from '../../services/services.service';
import {LoadingService } from '../../services/loading.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Base64 } from '@ionic-native/base64/ngx';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss'],
})
export class NewComponent implements OnInit {
  //  fileArray: Array<{ displayFile: any; base64File: string }> = [];
  //  imageArr: Array<{ displayImg: any; base64Img: string }> = [];
  @ViewChild(MultiFileUploadComponent, {static: false}) fileField: MultiFileUploadComponent;

  // workorder:any = {};
  // departments:any;
  // priorities:any;
  // public workOrderForm: FormGroup;
  // userdetail:any;

  constructor(public svs: ServicesService, public formBuilder: FormBuilder, public loadingCtrl: LoadingService, public alertCtrl: AlertControllerService, public router: Router, private base64: Base64, public navCtrl: NavController) {
  //   this.workOrderForm = formBuilder.group({
  //     department: ['', Validators.compose([Validators.required])],
  //     priority: ['',Validators.compose([Validators.required])],
  //     description: ['',Validators.compose([Validators.required])] 
  // });
}

  ngOnInit() {
    // this.getDepartment();
    // this.getPriority();
    // this.getUserDetail();
  }

  // getDepartment(){
  //   this.loadingCtrl.present();
  //   this.svs.getDepartment().subscribe(async (res) => {
  //     this.departments = res;
  //     console.log(res);
  //     this.loadingCtrl.dismiss();
  //     }, err => {
  //       console.log(err);
  //       this.loadingCtrl.dismiss();
  //     })
  // }

  // getPriority(){
  //   this.loadingCtrl.present();
  //   this.svs.getPriority().subscribe(async (res) => {
  //     this.priorities = res;
  //     console.log(res);
  //     this.loadingCtrl.dismiss();
  //     }, err => {
  //       console.log(err);
  //       this.loadingCtrl.dismiss();
  //     })
  // }

  // addWorkOrder(){
  //   if(this.workOrderForm.valid){
  //     let files = this.fileField.getFiles();
  //     console.log(files);
  //     let formData = new FormData();
  //     formData.append('priority_id', this.workorder.priority);
  //     formData.append('description', this.workorder.description);
  //     formData.append('department_id', this.workorder.department);
  //     if(this.userdetail.branch_id == null){
  //       formData.append('created_by_branch_id', '1');
  //     }
  //     else{
  //       formData.append('created_by_branch_id', this.userdetail.branch_id);
  //     }
      
  //     files.forEach((file) => {
  //       let base64File = '';
  //       // formData.append('files[]', file.rawFile, file.name);
  //       // console.log(file.rawFile);
  //       this.base64.encodeFile(file.name).then((base64File: string) => {
  //         alert(base64File);
  //         console.log(base64File);
  //       }, (err) => {
  //         console.log(err);
  //       });
  //       formData.append('files[]',base64File)
  //     });
      
  //   console.log(formData);
  //   this.loadingCtrl.present();
  //   this.svs.addWorkOrder(formData).subscribe(async (res) => {
  //     console.log(res);
  //     this.loadingCtrl.dismiss();
  //     }, err => {
  //       console.log(err);
  //       this.loadingCtrl.dismiss();
  //     })

  //   }else{
  //     this.alertCtrl.presentAlert('Required', 'All Fields are required');
  //   }
  // }

  // addWorkOrder(){
  //   var fileArrayBase64 = "";
  //   var imageArrayBase64 = ""
  //   for(var i=0;i<this.fileArray.length;i++){
  //     if(fileArrayBase64 == ""){
  //       fileArrayBase64 = this.fileArray[i].base64File;
  //     }else{
  //       fileArrayBase64 = fileArrayBase64 +"," + this.fileArray[i].base64File;
  //     }
  //   }
  //   for(var j=0;j<this.imageArr.length;j++){
  //     if(imageArrayBase64 == ""){
  //       imageArrayBase64 = this.imageArr[j].base64Img;
  //     }else{
  //       imageArrayBase64 = imageArrayBase64  +"," + this.imageArr[j].base64Img;
  //     }
  //   }
  //   //debugger;

  //   var formData = new FormData();
  //   formData.append("fileArray", imageArrayBase64);
  //   formData.append('priority_id', this.workorder.priority);
  //   formData.append('description', this.workorder.description);
  //   formData.append('department_id', this.workorder.department);
  //   if(this.userdetail.branch_id == null){
  //   formData.append('created_by_branch_id', '1');
  //   }
  //   else{
  //   formData.append('created_by_branch_id', this.userdetail.branch_id);
  //   }
  //   console.log(formData);
  //   this.loadingCtrl.present();
  //    this.svs.addWorkOrder(formData).subscribe(async (res) => {
  //      console.log(res);
  //      this.loadingCtrl.dismiss();
  //      }, err => {
  //        console.log(err);
  //        this.loadingCtrl.dismiss();
  //      })
  // }

  // getUserDetail(){
  //   this.svs.getUserDetail().subscribe(async (res) => {
  //     this.userdetail = res;
  //     console.log(this.userdetail.branch_id);
  //     }, err => {
  //       console.log(err);
  //     })
  // }

}