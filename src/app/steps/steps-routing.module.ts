import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SteponeComponent } from './stepone/stepone.component';
import { SteptwoComponent } from './steptwo/steptwo.component';
import { StepthreeComponent } from './stepthree/stepthree.component';
import { StepfourComponent } from './stepfour/stepfour.component';

const routes: Routes = [
  {
  path: 'stepone/:business',
  component: SteponeComponent
  },
  {
    path: 'steptwo',
    component: SteptwoComponent
  },
  {
    path: 'stepthree',
    component: StepthreeComponent
  },
  {
    path: 'stepfour',
    component: StepfourComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StepsRoutingModule { }
