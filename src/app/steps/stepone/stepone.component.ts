import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertControllerService } from 'src/app/services/alert-controller.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-stepone',
  templateUrl: './stepone.component.html',
  styleUrls: ['./stepone.component.scss'],
})
export class SteponeComponent implements OnInit {

  businessName:any = {'business_name':''};
  business_name:any;
  business:any;
  userdetail: any;

  constructor(public navCtrl:NavController, public router:Router, public alertCtrl:AlertControllerService, public loadingCtrl:LoadingService, public svs:ServicesService, public activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.business = this.activatedRoute.snapshot.paramMap.get('business');
    this.business_name = this.business;
    //this.getUserDetail();
  }

  // addBusiness(){
  //   if(!this.business_name){
  //     this.alertCtrl.presentAlert('Required','Please add your business name');
  //   }else{
  //     this.businessNames.push(this.business_name);
  //     this.business_name = '';
  //   }
  // }

  async Next(){
    if(!this.business_name){
        this.alertCtrl.presentAlert('Required','Please add your business name');
    }else{
        this.businessName.business_name = this.business_name;
           await this.loadingCtrl.present();
            this.svs.stepTwo(this.businessName).subscribe(async (res) => {
            console.log(res);
            this.loadingCtrl.dismiss();
            this.router.navigate(['steps/steptwo']);
          }, err => {
            console.log(err);
            this.loadingCtrl.dismiss();
        })
      //this.router.navigate(['register/stepthree']);
    }
  }

  async getUserDetail(){
    await this.loadingCtrl.present();
    this.svs.getUserDetail().subscribe(async (res) => {
      this.userdetail = res;
      this.svs.userdetail = this.userdetail;
      console.log('ud' + JSON.stringify(this.userdetail));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

}
