import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AlertControllerService } from 'src/app/services/alert-controller.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-steptwo',
  templateUrl: './steptwo.component.html',
  styleUrls: ['./steptwo.component.scss'],
})
export class SteptwoComponent implements OnInit {
  departments:any;
  department:any;
  depData:any ={'name':''};

  constructor(public navCtrl:NavController, public router:Router, public alertCtrl:AlertControllerService, public loadingCtrl:LoadingService, public svs:ServicesService) { }

  ngOnInit() {
    this.getDepartments();
  }

 async addDepartment(){
    if(!this.department){
      this.alertCtrl.presentAlert('Required','Please add your department');
    }else{
      this.depData.name = this.department;
      await this.loadingCtrl.present();
       this.svs.addDepartment(this.depData).subscribe(async (res) => {
       console.log(res);
       this.department = '';
       this.getDepartments();
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
   })
    }
  }

  Next(){
    this.router.navigate(['steps/stepthree'])
  }

  async getDepartments(){
    await this.loadingCtrl.present();
    this.svs.getDepartments().subscribe(async (res) => {
      this.departments = res;
      console.log('Dep - ' + JSON.stringify(res));
      await this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
      })
  }

}
