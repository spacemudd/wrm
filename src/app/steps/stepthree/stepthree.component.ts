import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AlertControllerService } from 'src/app/services/alert-controller.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-stepthree',
  templateUrl: './stepthree.component.html',
  styleUrls: ['./stepthree.component.scss'],
})
export class StepthreeComponent implements OnInit {
  branches:any;
  branch:any;
  branchData:any = {'name':''};
  constructor(public navCtrl:NavController, public router:Router, public alertCtrl:AlertControllerService, public loadingCtrl:LoadingService, public svs:ServicesService) { }

  ngOnInit() {
    this.getBranches();
  }

  async addBranch(){
    if(!this.branch){
      this.alertCtrl.presentAlert('Required','Please add your branch');
    }else{
      this.branchData.name = this.branch;
      await this.loadingCtrl.present();
       this.svs.addBranch(this.branchData).subscribe(async (res) => {
       console.log(res);
       this.branch = '';
       this.getBranches();
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
    })
   }
  }

  async Finish(){
      await this.loadingCtrl.present();
       this.svs.finishOnboardedAt().subscribe(async (res) => {
       console.log(res);
       this.loadingCtrl.dismiss();
       this.router.navigate(['steps/stepfour']);
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
    })
  }

  async getBranches(){
    await this.loadingCtrl.present();
    this.svs.getBranches().subscribe(async (res) => {
      this.branches = res;
      console.log('Branches - ' + JSON.stringify(res));
      await this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
      })
  }

}
