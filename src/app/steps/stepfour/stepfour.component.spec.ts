import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StepfourComponent } from './stepfour.component';

describe('StepfourComponent', () => {
  let component: StepfourComponent;
  let fixture: ComponentFixture<StepfourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepfourComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StepfourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
