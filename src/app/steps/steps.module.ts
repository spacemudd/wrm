import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SteponeComponent } from './stepone/stepone.component';
import { SteptwoComponent } from './steptwo/steptwo.component';
import { StepthreeComponent } from './stepthree/stepthree.component';
import { StepsRoutingModule } from './steps-routing.module';
import { StepfourComponent } from './stepfour/stepfour.component';




@NgModule({
  imports: [
    CommonModule,
    StepsRoutingModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule
  ],
  declarations: [SteponeComponent,SteptwoComponent,StepthreeComponent, StepfourComponent]
})
export class StepsModule { }
