import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ServicesService } from 'src/app/services/services.service';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertControllerService } from 'src/app/services/alert-controller.service';

@Component({
  selector: 'app-tenants',
  templateUrl: './tenants.component.html',
  styleUrls: ['./tenants.component.scss'],
})
export class TenantsComponent implements OnInit {
tenents:any;
pages:any =[];
current_page:any = 1 ;
last_page:any;
  constructor(public navCtrl:NavController, public router:Router, public loadingCtrl:LoadingService, public svs:ServicesService, public alertCtrl:AlertControllerService) { }

  ngOnInit() {
    this.getTenantsList(1);
  }

  getTenantsList(page){
    this.loadingCtrl.present();
    this.svs.getTenantsList(page).subscribe(async (res) => {
      this.tenents = res.data;
      this.last_page = res.last_page;
      console.log('Ten - ' + JSON.stringify(res));
      this.setPagination();
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  detail(id:any){
    this.router.navigate(['settings/tenentdetail/' + id]);
  }

 async deleteTenent(id){
   let con = await  this.alertCtrl.deleteConfirm('Confirm' , 'Do you want to delete this record');
   await con.present();
   con.onDidDismiss().then(res=>{
     if(res.data == true){
      this.loadingCtrl.present();
      this.svs.deleteTenent(id).subscribe(async (res) => {
      console.log(JSON.stringify(res));
      this.loadingCtrl.dismiss();
    }, err => {
      console.log(err);
      this.loadingCtrl.dismiss();
  })
     }else{
       
     }
   })
  }

  setPagination(){
    for(let i =1; i<= this.last_page; i++){
      this.pages.push(i);
      console.log('pages - ' + this.pages);
    }
  }

  goToPage(page){
    this.current_page = page;
    this.getTenantsListBypage(page);
  }

  getTenantsListBypage(page){
    this.loadingCtrl.present();
    this.svs.getTenantsList(page).subscribe(async (res) => {
      this.tenents = res.data;
      console.log('Ten - ' + JSON.stringify(res));
      this.setPagination();
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }


}
