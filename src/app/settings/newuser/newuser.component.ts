import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services/services.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';
import { AlertControllerService } from 'src/app/services/alert-controller.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.scss'],
})
export class NewuserComponent implements OnInit {
  user:any ={};
  public registerForm: FormGroup;
  userdata: any;
  roles:any;
  branches:any;
  departments:any;
  constructor(public svs: ServicesService, public formBuilder: FormBuilder, public arouter:ActivatedRoute,  public loadingCtrl: LoadingService, public alertCtrl: AlertControllerService, public router: Router, public navCtrl:NavController) {
    this.registerForm = formBuilder.group({
      name:['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      number:['',Validators.compose([Validators.required])],
      password: ['',Validators.compose([Validators.required])],
      branch_id:[''],
      department_id:[''],
      role_id:['']
    });
   }

  ngOnInit() {
    this.getRoles();
    this.getBranches();
    this.getDepartments();
  }

  async getRoles(){
      await this.loadingCtrl.present();
      this.svs.getRoles().subscribe(async (res) => {
        this.roles = res.roles;
        console.log(JSON.stringify(res));
        this.loadingCtrl.dismiss();
        }, err => {
          console.log(err);
          this.loadingCtrl.dismiss();
      })
  }

 getBranches(){
    this.svs.getBranches().subscribe(async (res) => {
      this.branches = res;
      console.log('Branches - ' + JSON.stringify(res));
      }, err => {
        console.log(err);
    })
}

getDepartments(){
  this.svs.getDepartments().subscribe(async (res) => {
    this.departments = res;
    console.log('Dep - ' + JSON.stringify(res));
    }, err => {
      console.log(err);
  })
}

async Save(){
  if(this.registerForm.valid){
    await this.loadingCtrl.present();
    this.svs.saveUser(this.user).subscribe(async (res) => {
      console.log('Res - ' + JSON.stringify(res));
      if(res){
        this.loadingCtrl.dismiss();
        this.alertCtrl.presentAlert('نجاح','تم إنشاء المستخدم بنجاح');
        this.router.navigate(['settings/users']);
      }
      }, err => {
        this.loadingCtrl.dismiss();
        console.log(err);
    })
  }else{
    this.alertCtrl.presentAlert('Required', 'All Fields are required');
  }
}

}