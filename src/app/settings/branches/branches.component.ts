import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';
import { Router } from '@angular/router';
import { AlertControllerService } from 'src/app/services/alert-controller.service';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.scss'],
})
export class BranchesComponent implements OnInit {
branches:any;
  constructor(public navCtrl:NavController, public loadingCtrl:LoadingService, public svs:ServicesService, public router:Router, public alertCtrl:AlertControllerService) { }

  ngOnInit() {
    //this.getBranches();
  }

  async getBranches(){
    await this.loadingCtrl.present();
    this.svs.getBranches().subscribe(async (res) => {
      this.branches = res;
      console.log('branches - ' + JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  addNew(){
    this.router.navigate(['settings/newbranch']);
  }

  ionViewWillEnter() {
    this.getBranches();
  }

  async deleteBranch(id){
    let con = await  this.alertCtrl.deleteConfirm('تأكيد' , 'هل انت متأكد من حذف الفرع؟');
    await con.present();
    con.onDidDismiss().then(res=>{
      if(res.data == true){
       this.loadingCtrl.present();
       this.svs.deleteBranch(id).subscribe(async (res) => {
       if(res){
        console.log(JSON.stringify(res));
        this.loadingCtrl.dismiss();
        let suc = await this.alertCtrl.successMessage('نجاح','تم بنجاح حذف الفرع');
        suc.present();
        suc.onDidDismiss().then(()=>{
         this.getBranches();
        })
       }
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
   })
      }else{
        
      }
    })
   }

   editBranch(id,name,name_en){
     this.router.navigate(['settings/editbranch/', id, name, name_en]);
   }

}
