import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';
import { AlertControllerService } from 'src/app/services/alert-controller.service';

@Component({
  selector: 'app-tenantdetail',
  templateUrl: './tenantdetail.component.html',
  styleUrls: ['./tenantdetail.component.scss'],
})
export class TenantdetailComponent implements OnInit {
id:any;
tenantData:any = {};
subscriptions:any = [];
  constructor(public navCtrl:NavController, public activatedRoute:ActivatedRoute, public loadingCtrl:LoadingService, public svs:ServicesService, public alertCtrl:AlertControllerService, public router:Router) { 
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getTenantData();
  }

  getTenantData(){
    this.loadingCtrl.present();
    this.svs.getTenantData(this.id).subscribe(async (res) => {
      this.tenantData = res;
      this.subscriptions =  res.subscriptions;
      console.log(JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
    })
  }
 
  async deleteSubscription(tid,sid){
    let con = await this.alertCtrl.deleteConfirm('Confirm' , 'Do you want to delete this record');
    await con.present();
    con.onDidDismiss().then(res=>{
      if(res.data == true){
        this.loadingCtrl.present();
        this.svs.deleteSubscription(tid,sid).subscribe(async (res) => {
        console.log(JSON.stringify(res));
        this.getTenantData();
        this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
    })
      }else{
        
      }
    })
  }

  addNew(tid){
    this.router.navigate(['settings/newsubscriptions', tid])
  }

}
