import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { IonicModule } from '@ionic/angular';
import { TenantsComponent } from './tenants/tenants.component';
import { TenantdetailComponent } from './tenantdetail/tenantdetail.component';
import { NewsubscriptionComponent } from './newsubscription/newsubscription.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { NewuserComponent } from './newuser/newuser.component';
import { RolesComponent } from './roles/roles.component';
import { BranchesComponent } from './branches/branches.component';
import { EdituserComponent } from './edituser/edituser.component';
import { NewbranchComponent } from './newbranch/newbranch.component';
import { EditbranchComponent } from './editbranch/editbranch.component';


@NgModule({
  declarations: [SettingsComponent, TenantsComponent, TenantdetailComponent, NewsubscriptionComponent, UsersComponent, NewuserComponent, RolesComponent, BranchesComponent, EdituserComponent, NewbranchComponent, EditbranchComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SettingsModule { }
