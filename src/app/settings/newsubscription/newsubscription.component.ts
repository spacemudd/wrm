import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';
import { AlertControllerService } from 'src/app/services/alert-controller.service';

@Component({
  selector: 'app-newsubscription',
  templateUrl: './newsubscription.component.html',
  styleUrls: ['./newsubscription.component.scss'],
})
export class NewsubscriptionComponent implements OnInit {
tid:any;
plans:any;
months:any;
plan:any;
  constructor(public activatedRoute:ActivatedRoute, public navCtrl:NavController, public loadingCtrl:LoadingService, public svs:ServicesService, public alertCtrl:AlertControllerService, public router:Router) { 
    this.tid = this.activatedRoute.snapshot.paramMap.get('tid');
  }

  ngOnInit() {
    this.getPlans();
  }

  getPlans(){
    this.loadingCtrl.present();
    this.svs.getPlans().subscribe(async (res) => {
      this.plans = res
      console.log(JSON.stringify(this.plans));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  addSubscription(){
   if(!this.months){
     this.alertCtrl.presentAlert('Required', 'Please enter no. of months');
   }else{
    this.loadingCtrl.present();
    this.svs.addSubscription(this.tid,this.plan,this.months).subscribe(async (res) => {
    console.log(JSON.stringify(res));
    this.loadingCtrl.dismiss();
    this.alertCtrl.presentAlert('نجاح', 'تم تسجيل اشتراك جديد بنجاح');
    this.router.navigate(['/settings/tenants']);
  }, err => {
    console.log(err);
    this.loadingCtrl.dismiss();
})
   }
  }

}
