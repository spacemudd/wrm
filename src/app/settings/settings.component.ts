import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {

  constructor(public navCtrl:NavController, public router:Router, public svs:ServicesService) { }

  ngOnInit() {}

  tenants(){
    this.router.navigate(['settings/tenants']);
  }

  users(){
    this.router.navigate(['settings/users']);
  }

  roles(){
    this.router.navigate(['settings/roles']);
  }

  branches(){
    this.router.navigate(['settings/branches']);
  }

}
