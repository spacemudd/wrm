import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { Route } from '@angular/compiler/src/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertControllerService } from 'src/app/services/alert-controller.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.scss'],
})
export class EdituserComponent implements OnInit {
  user:any ={};
  public registerForm: FormGroup;
  roles:any;
  id:any;
  name:any;
  email:any;
  role_id:any;
  number:any;

  constructor(public svs: ServicesService, public formBuilder: FormBuilder, public loadingCtrl: LoadingService, public alertCtrl: AlertControllerService, public router:Router, public navCtrl:NavController, public activatedRoute:ActivatedRoute) {
    this.registerForm = formBuilder.group({
      name:['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      number:['',Validators.compose([Validators.required])],
      role_id:['']
    });
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.name = this.activatedRoute.snapshot.paramMap.get('name');
    this.email = this.activatedRoute.snapshot.paramMap.get('email');
    this.role_id = this.activatedRoute.snapshot.paramMap.get('role_id');
    this.number = this.activatedRoute.snapshot.paramMap.get('number');
    this.user.name = this.name;
    this.user.email = this.email;
    this.user.role_id = this.role_id;
    this.user.number = this.number;
   }

  ngOnInit() {
   this.getRoles();
  }

  async getRoles(){
    await this.loadingCtrl.present();
    this.svs.getRoles().subscribe(async (res) => {
      this.roles = res.roles;
      console.log(JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
    })
  }

async updateUser(){
  await this.loadingCtrl.present();
    this.svs.updateUser(this.id, this.user).subscribe(async (res) => {
      console.log(JSON.stringify(res));
      this.loadingCtrl.dismiss();
      if(res){
        this.alertCtrl.presentAlert('success', 'user updated successfully');
      }
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
    })
}

}