import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './settings.component';
import { TenantsComponent } from './tenants/tenants.component';
import { TenantdetailComponent } from './tenantdetail/tenantdetail.component';
import { NewsubscriptionComponent } from './newsubscription/newsubscription.component';
import { UsersComponent } from './users/users.component';
import { NewuserComponent } from './newuser/newuser.component';
import { RolesComponent } from './roles/roles.component';
import { BranchesComponent } from './branches/branches.component';
import { EdituserComponent } from './edituser/edituser.component';
import { NewbranchComponent } from './newbranch/newbranch.component';
import { EditbranchComponent } from './editbranch/editbranch.component';


const routes: Routes = [
  {
    path:'',
    component:SettingsComponent
  },
  {
    path:'tenants',
    component:TenantsComponent
  },
  {
    path:'tenentdetail/:id',
    component:TenantdetailComponent
  },
  {
    path:'newsubscriptions/:tid',
    component:NewsubscriptionComponent
  },
  {
    path:'users',
    component:UsersComponent
  },
  {
    path:'newuser',
    component:NewuserComponent
  },
  {
    path:'roles',
    component:RolesComponent
  },
  {
    path:'branches',
    component:BranchesComponent
  },
  {
    path:'edituser/:id/:name/:email/:role_id/:number',
    component:EdituserComponent
  },
  {
    path:'newbranch',
    component:NewbranchComponent
  },
  {
    path:'editbranch/:id/:name/:name_en',
    component:EditbranchComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
