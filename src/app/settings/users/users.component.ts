import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';
import { AlertControllerService } from 'src/app/services/alert-controller.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
users:any;
  constructor(public navCtrl:NavController, public router:Router, public loadingCtrl:LoadingService, public svs:ServicesService, public alertCtrl:AlertControllerService) { }

  ngOnInit() {
    //this.getUsersList();
  }

  ionViewWillEnter() {
    this.getUsersList();
  }

  async getUsersList(){
    await this.loadingCtrl.present();
    this.svs.getUsersList().subscribe(async (res) => {
      this.users = res;
      console.log('Users - ' + JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  addNew(){
    this.router.navigate(['settings/newuser']);
  }

  editUser(id,name,email,role_id,number){
    this.router.navigate(['settings/edituser/' + id,name,email,role_id,number]);
  }

  async deleteUser(id){
    let con = await  this.alertCtrl.deleteConfirm('تأكيد' , 'هل انت متأكد من حذف المستخدم؟');
    await con.present();
    con.onDidDismiss().then(res=>{
      if(res.data == true){
       this.loadingCtrl.present();
       this.svs.deleteUser(id).subscribe(async (res) => {
       if(res){
        console.log(JSON.stringify(res));
        this.loadingCtrl.dismiss();
        let suc = await this.alertCtrl.successMessage('نجاح','تم حذف المستخدم بنجاح');
        suc.present();
        suc.onDidDismiss().then(()=>{
         this.getUsersList();
        })
       }
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
   })
      }else{
        
      }
    })
   }
  

}