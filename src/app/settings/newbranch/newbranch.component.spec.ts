import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewbranchComponent } from './newbranch.component';

describe('NewbranchComponent', () => {
  let component: NewbranchComponent;
  let fixture: ComponentFixture<NewbranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewbranchComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewbranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
