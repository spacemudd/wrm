import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AlertControllerService } from 'src/app/services/alert-controller.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-newbranch',
  templateUrl: './newbranch.component.html',
  styleUrls: ['./newbranch.component.scss'],
})
export class NewbranchComponent implements OnInit {
  name:any;
  name_en:any;
  branchData:any = {'name':'','name_en':''};
  constructor(public navCtrl:NavController, public router:Router, public alertCtrl:AlertControllerService, public loadingCtrl:LoadingService, public svs:ServicesService) { }

  ngOnInit() {}

  async addNewBranch(){
    if(!this.name){
      this.alertCtrl.presentAlert('Required','Please enter branch name');
    }else{
      this.branchData.name = this.name;
      this.branchData.name_en = this.name_en;
      await this.loadingCtrl.present();
       this.svs.addBranch(this.branchData).subscribe(async (res) => {
       if(res){
        console.log(res);
        this.name = '';
        this.name_en= '';
        this.loadingCtrl.dismiss();
        this.alertCtrl.presentAlert('نجاح','تم حفظ الفرع بنجاح');
        this.router.navigate(['settings/branches']);
       }
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
    })
   }
  }

}
