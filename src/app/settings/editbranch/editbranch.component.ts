import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AlertControllerService } from 'src/app/services/alert-controller.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editbranch',
  templateUrl: './editbranch.component.html',
  styleUrls: ['./editbranch.component.scss'],
})
export class EditbranchComponent implements OnInit {
  id:any;
  name:any;
  name_en:any;
  branchData:any = {'name':'','name_en':''};
  constructor(public navCtrl:NavController, public alertCtrl:AlertControllerService, public loadingCtrl:LoadingService, public svs:ServicesService, public activatedRoute:ActivatedRoute, public router:Router) { 
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.name = this.activatedRoute.snapshot.paramMap.get('name');
    this.name_en = this.activatedRoute.snapshot.paramMap.get('name_en');
  }

  ngOnInit() {
    
  }

  async updateBranch(){
    if(!this.name){
      this.alertCtrl.presentAlert('Required','Please enter branch name');
    }else{
      this.branchData.name = this.name;
      this.branchData.name_en = this.name_en;
      await this.loadingCtrl.present();
       this.svs.updateBranch(this.id , this.branchData).subscribe(async (res) => {
       if(res && res!=null){
        console.log(res);
        this.loadingCtrl.dismiss();
        this.alertCtrl.presentAlert('success','branch updated successfully');
        this.router.navigate(['settings/branches']);
       }
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
    })
   }
  }

}
