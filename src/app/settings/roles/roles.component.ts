import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';
import { AlertControllerService } from 'src/app/services/alert-controller.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
})
export class RolesComponent implements OnInit {
roles:any;
roleData:any = {'role_id':'', 'permission_name':''}
  constructor(public navCtrl:NavController, public loadingCtrl:LoadingService, public svs:ServicesService, public alertCtrl:AlertControllerService) { }
  ngOnInit() {
    this.getRoles();
  }

  async getRoles(){
    await this.loadingCtrl.present();
    this.svs.getRoles().subscribe(async (res) => {
      this.roles = res.roles;
      console.log('roles - ' + JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  async checkPermission(c:any, role_id:any, permission_name:any){
    this.roleData.role_id = role_id;
    this.roleData.permission_name = permission_name;
    if(c==false){
      await this.loadingCtrl.present();
       this.svs.attachPermission(this.roleData).subscribe(async (res) => {
       if(res){
        console.log(res);
        this.loadingCtrl.dismiss();
        this.getRoles();
       }
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
    })
    }else{
      await this.loadingCtrl.present();
       this.svs.detachPermission(this.roleData).subscribe(async (res) => {
       if(res){
        console.log(res);
        this.loadingCtrl.dismiss();
        this.getRoles();
       }
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
    })
    }
  }

}