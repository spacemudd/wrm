import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-departmentreport',
  templateUrl: './departmentreport.component.html',
  styleUrls: ['./departmentreport.component.scss'],
})
export class DepartmentreportComponent implements OnInit {
  departmentreport:any;
  departments:any;
  date_start:any;
  date_end:any;
  department:any;
  refreshData:any = {date_start:'', date_end:'', department_id:''};
  constructor(public navCtrl:NavController, public loadingCtrl:LoadingService, public svs:ServicesService) { }

  ngOnInit() {
    this.getDepartmentReport();
  }

 async getDepartmentReport(){
   await this.loadingCtrl.present();
    this.svs.getDepartmentReport().subscribe(async (res) => {
      this.departmentreport = res.departments;
      console.log(JSON.stringify(this.departmentreport));
      this.getDepartments();
        this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  getDepartments(){
    this.svs.getDepartments().subscribe(async (res) => {
      this.departments = res;
      console.log(JSON.stringify(res));
      }, err => {
        console.log(err);
      })
  }

  async refresDepartmentReport(){
    if(this.date_start){
      this.refreshData.date_start = this.date_start.split('T')[0].split("-").reverse().join("-");
    }
    if(this.date_end){
      this.refreshData.date_end = this.date_end.split('T')[0].split("-").reverse().join("-");
    }
    if(this.department){
      this.refreshData.department_id = this.department;
    }
    await this.loadingCtrl.present();
    this.svs.refresDepartmentReport(this.refreshData).subscribe(async (res) => {
      this.departmentreport = res.departments;
      console.log(JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

}
