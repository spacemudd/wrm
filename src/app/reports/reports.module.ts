import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports.component';
import { IonicModule } from '@ionic/angular';
import { WorkorderreportComponent } from './workorderreport/workorderreport.component';
import { ExecutivedirectorreportComponent } from './executivedirectorreport/executivedirectorreport.component';
import { BranchreportComponent } from './branchreport/branchreport.component';
import { DepartmentreportComponent } from './departmentreport/departmentreport.component';
import { PresidentreportComponent } from './presidentreport/presidentreport.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ReportsComponent,WorkorderreportComponent,ExecutivedirectorreportComponent,BranchreportComponent,DepartmentreportComponent,PresidentreportComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    IonicModule,
    FormsModule
  ]
})
export class ReportsModule { }
