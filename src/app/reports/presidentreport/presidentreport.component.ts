import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-presidentreport',
  templateUrl: './presidentreport.component.html',
  styleUrls: ['./presidentreport.component.scss'],
})
export class PresidentreportComponent implements OnInit {
  presidentreport:any;
  date_start:any;
  date_end:any;
  refreshData:any = {date_start:'', date_end:''};
  constructor(public navCtrl:NavController, public loadingCtrl:LoadingService, public svs:ServicesService) { }

  ngOnInit() {
    this.getPresidentReport();
  }

  async getPresidentReport(){
    await this.loadingCtrl.present();
     this.svs.getPresidentReport().subscribe(async (res) => {
       console.log(JSON.stringify(res));
         this.loadingCtrl.dismiss();
       }, err => {
         console.log(err);
         this.loadingCtrl.dismiss();
       })
   }

   async refresPresidentReport(){
    if(this.date_start){
      this.refreshData.date_start = this.date_start.split('T')[0].split("-").reverse().join("-");
    }
    if(this.date_end){
      this.refreshData.date_end = this.date_end.split('T')[0].split("-").reverse().join("-");
    }
    await this.loadingCtrl.present();
    this.svs.refresPresidentReport(this.refreshData).subscribe(async (res) => {
      this.presidentreport = res.departments;
      console.log(JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

}
