import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { WorkorderreportComponent } from './workorderreport/workorderreport.component';
import { ExecutivedirectorreportComponent } from './executivedirectorreport/executivedirectorreport.component';
import { BranchreportComponent } from './branchreport/branchreport.component';
import { DepartmentreportComponent } from './departmentreport/departmentreport.component';
import { PresidentreportComponent } from './presidentreport/presidentreport.component';


const routes: Routes = [
  {
    path:'',
    component:ReportsComponent
  },
  {
    path:'workorderreport',
    component:WorkorderreportComponent
  },
  {
    path:'executivedirectorreport',
    component:ExecutivedirectorreportComponent
  },
  {
    path:'branchreport',
    component:BranchreportComponent
  },
  {
    path:'departmentreport',
    component:DepartmentreportComponent
  },
  {
    path:'presidentreport',
    component:PresidentreportComponent
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
