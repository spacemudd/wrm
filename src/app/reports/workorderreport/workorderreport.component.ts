import { Component, OnInit, ModuleWithComponentFactories } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-workorderreport',
  templateUrl: './workorderreport.component.html',
  styleUrls: ['./workorderreport.component.scss'],
})
export class WorkorderreportComponent implements OnInit {
workorderreport:any;
branches:any;
statuses:any;
date_start:any;
date_end:any;
branch_id:any;
status:any;

refreshData:any = {date_start:'', date_end:'', branch_id:'', status:''};
  constructor(public navCtrl:NavController, public loadingCtrl:LoadingService, public svs:ServicesService) { }

  ngOnInit() {
    this.getWorkOrderReport();
    this.getBranches();
    this.getStatus();
  }

  getWorkOrderReport(){
    this.loadingCtrl.present();
    this.svs.getWorkOrderReport().subscribe(async (res) => {
      this.workorderreport = res.workOrders;
      console.log(JSON.stringify(this.workorderreport));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  getBranches(){
    this.svs.getBranches().subscribe(async (res) => {
      this.branches = res
     // console.log('Branches - ' + JSON.stringify(res));
      }, err => {
        console.log(err);
      })
  }

  getStatus(){
    this.svs.getStatus().subscribe(async (res) => {
      this.statuses = res
      //console.log('status - ' + JSON.stringify(res));
      }, err => {
        console.log(err);
    })
  }

  async refreshWorkOrderReport(){
    if(this.date_start){
      this.refreshData.date_start = this.date_start.split('T')[0].split("-").reverse().join("-");
    }
    if(this.date_end){
      this.refreshData.date_end = this.date_end.split('T')[0].split("-").reverse().join("-");
    }
    if(this.branch_id){
      this.refreshData.branch_id = this.branch_id;
    }
    if(this.status){
      this.refreshData.status = this.status;
    }
    await this.loadingCtrl.present();
    this.svs.refreshWorkOrderReport(this.refreshData).subscribe(async (res) => {
      this.workorderreport = res.workOrders;
      console.log(JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })

  }

}
