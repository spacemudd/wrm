import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-branchreport',
  templateUrl: './branchreport.component.html',
  styleUrls: ['./branchreport.component.scss'],
})
export class BranchreportComponent implements OnInit {
  branchreport:any;
  branches:any;
  date_start:any;
  date_end:any;
  branch_id:any;
  refreshData:any = {date_start:'', date_end:'', branch_id:''};

  constructor(public navCtrl:NavController, public loadingCtrl:LoadingService, public svs:ServicesService) { }

  ngOnInit() {
    this.getBranchReport();
    this.getBranches();
  }

  filterReports(){
    
  }

  getBranchReport(){
    this.loadingCtrl.present();
    this.svs.getBranchReport().subscribe(async (res) => {
      this.branchreport = res.data;
      console.log(JSON.stringify(res.data));
        this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  getBranches(){
    this.svs.getBranches().subscribe(async (res) => {
      this.branches = res
     // console.log('Branches - ' + JSON.stringify(res));
      }, err => {
        console.log(err);
      })
  }

  async refresBranchReport(){
    if(this.date_start){
      this.refreshData.date_start = this.date_start.split('T')[0].split("-").reverse().join("-");
    }
    if(this.date_end){
      this.refreshData.date_end = this.date_end.split('T')[0].split("-").reverse().join("-");
    }
    if(this.branch_id){
      this.refreshData.branch_id = this.branch_id;
    }
    await this.loadingCtrl.present();
    this.svs.refresBranchReport(this.refreshData).subscribe(async (res) => {
      this.branchreport = res.data;
      console.log(JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })

  }

}
