import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BranchreportComponent } from './branchreport.component';

describe('BranchreportComponent', () => {
  let component: BranchreportComponent;
  let fixture: ComponentFixture<BranchreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchreportComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BranchreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
