import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {

  constructor(public navCtrl: NavController, public router:Router) { }

  ngOnInit() {}

  WorkorderReport(){
    this.router.navigate(['/reports/workorderreport']);
  }
  ExecutiveReport(){
    this.router.navigate(['reports/executivedirectorreport']);
  }
  BranchReport(){
    this.router.navigate(['reports/branchreport']);
  }
  DepartmentReport(){
    this.router.navigate(['reports/departmentreport']);
  }
  // PresidentReport(){
  //   this.router.navigate(['reports/presidentreport']);
  // }

}