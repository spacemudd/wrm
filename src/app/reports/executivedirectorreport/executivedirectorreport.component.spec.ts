import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExecutivedirectorreportComponent } from './executivedirectorreport.component';

describe('ExecutivedirectorreportComponent', () => {
  let component: ExecutivedirectorreportComponent;
  let fixture: ComponentFixture<ExecutivedirectorreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutivedirectorreportComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExecutivedirectorreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
