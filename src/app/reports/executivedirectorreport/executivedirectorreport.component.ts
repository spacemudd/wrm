import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-executivedirectorreport',
  templateUrl: './executivedirectorreport.component.html',
  styleUrls: ['./executivedirectorreport.component.scss'],
})
export class ExecutivedirectorreportComponent implements OnInit {
  executivereport:any;
  date_start:any;
  date_end:any;
  refreshData:any = {date_start:'', date_end:''};
  constructor(public navCtrl:NavController, public loadingCtrl:LoadingService, public svs:ServicesService) { }

  ngOnInit() {
    this.getExecutiveReport();
  }

  getExecutiveReport(){
    this.loadingCtrl.present();
    this.svs.getExecutiveReport().subscribe(async (res) => {
      this.executivereport = res;
      console.log('ER - ' + JSON.stringify(res));
        this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  async refresExecutiveReport(){
    if(this.date_start){
      this.refreshData.date_start = this.date_start.split('T')[0].split("-").reverse().join("-");
    }
    if(this.date_end){
      this.refreshData.date_end = this.date_end.split('T')[0].split("-").reverse().join("-");
    }
    await this.loadingCtrl.present();
    this.svs.refresExecutiveReport(this.refreshData).subscribe(async (res) => {
      this.executivereport = res;
      console.log(JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

}
