import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepartmentsComponent } from './departments.component';
import { NewComponent } from './new/new.component';


const routes: Routes = [
  {
    path:'',
    component:DepartmentsComponent
  },
  {
    path:'new',
    component:NewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentsRoutingModule { }
