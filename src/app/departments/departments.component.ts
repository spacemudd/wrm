import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { ServicesService } from '../services/services.service';
import { AlertControllerService } from '../services/alert-controller.service';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.scss'],
})
export class DepartmentsComponent implements OnInit {
  departments:any;
  constructor(public navCtrl:NavController, public router:Router, public loadingCtrl:LoadingService, public svs: ServicesService, public alertCtrl:AlertControllerService) { }

  ngOnInit() {
    //this.getDepartments();
  }

  addNew(){
    this.router.navigate(['departments/new']);
  }

  async getDepartments(){
    await this.loadingCtrl.present();
    this.svs.getDepartments().subscribe(async (res) => {
      this.departments = res;
      console.log('Dep - ' + JSON.stringify(res));
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  async deleteDepartment(id){
    let con = await  this.alertCtrl.deleteConfirm('تأكيد' , 'هل انت متأكد من حذف هذا الفرع');
    await con.present();
    con.onDidDismiss().then(res=>{
      if(res.data == true){
       this.loadingCtrl.present();
       this.svs.deleteDepartment(id).subscribe(async (res) => {
       console.log(JSON.stringify(res));
       this.loadingCtrl.dismiss();
       this.getDepartments();
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
   })
      }else{
        
      }
    })
   }

   ionViewWillEnter() {
    this.getDepartments();
  }

}
