import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartmentsRoutingModule } from './departments-routing.module';
import { IonicModule } from '@ionic/angular';
import { DepartmentsComponent } from './departments.component';
import { NewComponent } from './new/new.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [DepartmentsComponent, NewComponent],
  imports: [
    CommonModule,
    DepartmentsRoutingModule,
    IonicModule,
    FormsModule
  ]
})
export class DepartmentsModule { }
