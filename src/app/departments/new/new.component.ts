import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AlertControllerService } from 'src/app/services/alert-controller.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss'],
})
export class NewComponent implements OnInit {
  name:any;
  name_en:any;
  depData:any ={'name':'','name_en':''};
  constructor(public navCtrl:NavController, public alertCtrl:AlertControllerService, public loadingCtrl:LoadingService, public svs: ServicesService) { }

  ngOnInit() {}

  async addDepartment(){
    if(!this.name){
      this.alertCtrl.presentAlert('Required','Please enter department name');
    }else{
      this.depData.name = this.name;
      this.depData.name_en = this.name_en;
      await this.loadingCtrl.present();
       this.svs.addDepartment(this.depData).subscribe(async (res) => {
       console.log(res);
       this.name = '';
       this.name_en = '';
       this.loadingCtrl.dismiss();
       this.alertCtrl.presentAlert('نجاح','"تم إضافة الإدارة بنجاح');
     }, err => {
       console.log(err);
       this.loadingCtrl.dismiss();
   })
    }
  }

}
