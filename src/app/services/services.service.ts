import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  userdetail:any;
  access_token:any = '';
  constructor(public http:HttpClient, public storage: Storage) { 
    
  }

  apipath:string = "https://saremquietrooms.com/api"
  

  addWorkOrder(data){
    //return this.http.post('http://192.168.0.108/imgUpload/upload.php',data);
    return this.http.post(this.apipath + '/work-orders', data);
  }


  getHomeData(): Observable<any> {
    return this.http.get(this.apipath + '/home')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getHomeData', []))
      );
  }
  forgotPass(email): Observable<any> {
    const headers = new HttpHeaders({
      'accept': 'application/json'     
    });
    const  params = new  HttpParams().set('email', email);
    return this.http.post(this.apipath + '/password/email', params,{headers:headers})
      .pipe(
        tap(_ => this.log('response received')),
       // catchError(this.handleError('getHomeData', []))
      );
  }
  resetPass(data): Observable<any> {
    const headers = new HttpHeaders({
      'accept': 'application/json'     
    });
    
    return this.http.post(this.apipath + '/password/reset', data,{headers:headers})
      .pipe(
        tap(_ => this.log('response received')),
       // catchError(this.handleError('getHomeData', []))
      );
  }

  getWorkorders(type, id, status): Observable<any> {
    if(type === 'executive'){
      return this.http.get(this.apipath + '/executive/work-orders/' + status)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getWorkorders', []))
      );
    } else if(type === 'president'){
      return this.http.get(this.apipath + '/president/work-orders')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getWorkorders', []))
      );
    }else if(type === 'departments'){
      return this.http.get(this.apipath + '/departments/' + id + '/work-orders/' + status)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getWorkorders', []))
      );
    }else if(type ==='branches'){
      return this.http.get(this.apipath + '/branches/' + id + '/work-orders')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getWorkorders', []))
      );
    }else{
      return this.http.get(this.apipath + '/work-orders')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getWorkorders', []))
      );
    }
    
  }

  getWorkordersById(id): Observable<any> {
    return this.http.get(this.apipath + '/work-orders/' + id)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getWorkordersById', []))
      );
  }

  completeWork(id, note): Observable<any> {
    const  params = new  HttpParams().set('note', note);
    return this.http.post(this.apipath + '/work-orders/' + id + '/complete', params)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('completeWork', []))
      );
  }

  escalateWork(id, note): Observable<any> {
    const  params = new  HttpParams().set('note', note);
    return this.http.post(this.apipath + '/work-orders/' + id + '/escalate', params)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('escalateWork', []))
      );
  }

  respondWorkOrder(id, note): Observable<any>{
    const  params = new  HttpParams().set('note', note);
    return this.http.post(this.apipath + '/work-orders/' + id + '/respond-to-returned', params)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('respondWorkOrder', []))
      );
  }

  returnWorkOrder(id, note): Observable<any>{
    const  params = new  HttpParams().set('note', note);
    return this.http.post(this.apipath + '/work-orders/' + id + '/return', params)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('respondWorkOrder', []))
      );
  }

  getDepartment(): Observable<any> {
    return this.http.get(this.apipath + '/departments')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getDepartment', []))
      );
  }

  getPriority(): Observable<any> {
    return this.http.get(this.apipath + '/priorities')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getPriority', []))
      );
  }

  // addWorkOrder(data): Observable<any> {
  //   //const  params = new  HttpParams().set('department_id', department).set('priority_id', priority).set('description', description).set('created_by_branch_id', branch_id).set('files', files);
  //   return this.http.post(this.apipath + '/work-orders', data)
  //     .pipe(
  //       tap(_ => this.log('response received')),
  //       catchError(this.handleError('getDetails', []))
  //     );
  // }

  getUserDetail(): Observable<any> {
    return this.http.get(this.apipath + '/me')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getUserDetail', []))
      );
  }

  getTenantsList(page): Observable<any> {
    return this.http.get(this.apipath + '/tenants?page =' + page)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getTenantsList', []))
      );
  }

  getTenantData(id): Observable<any> {
    return this.http.get(this.apipath + '/tenants/' + id)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getTenantData', []))
      );
  }

  deleteTenent(id): Observable<any> {
    return this.http.delete(this.apipath + '/tenants/' + id)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('deleteTenent', []))
      );
  }

  deleteSubscription(tid,sid): Observable<any> {
    return this.http.delete(this.apipath + '/tenants/' + tid + '/subscriptions/' + sid)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('deleteSubscription', []))
      );
  }

  getPlans(): Observable<any> {
    return this.http.get(this.apipath + '/plans')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getPlans', []))
      );
  }

  addSubscription(tid,plan,months): Observable<any> {
    const params = new  HttpParams().set('plan_id', plan).set('months', months);
    return this.http.post(this.apipath + '/tenants/' + tid + '/subscriptions/store', params)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('addSubscription', []))
      );
  }
  
  getBranches(): Observable<any> {
    return this.http.get(this.apipath + '/branches')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getBranches', []))
      );
  }

  getStatus(): Observable<any> {
    return this.http.get(this.apipath + '/statuses')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getStatus', []))
      );
  }

  getWorkOrderReport(): Observable<any> {
    return this.http.get(this.apipath + '/reports/work-orders')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getWorkOrderReport', []))
      );
  }

  refreshWorkOrderReport(refreshData): Observable<any> {
    return this.http.get(this.apipath + '/reports/work-orders?date_start=' + refreshData.date_start + '&date_end=' + refreshData.date_end + '&branch_id=' + refreshData.branch_id + '&status=' + refreshData.status)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('refreshWorkOrderReport', []))
      );
  }

  getBranchReport(): Observable<any> {
    return this.http.get(this.apipath + '/reports/branches')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getBranchReport', []))
      );
  }

  refresBranchReport(refreshData): Observable<any> {
    return this.http.get(this.apipath + '/reports/branches?date_start=' + refreshData.date_start + '&date_end=' + refreshData.date_end + '&branch_id=' + refreshData.branch_id)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('refreshWorkOrderReport', []))
      );
  }

  getExecutiveReport(): Observable<any> {
    return this.http.get(this.apipath + '/reports/executive-director')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getExecutiveReport', []))
      );
  }

  refresExecutiveReport(refreshData): Observable<any> {
    return this.http.get(this.apipath + '/reports/executive-director?date_start=' + refreshData.date_start + '&date_end=' + refreshData.date_end)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('refresExecutiveReport', []))
      );
  }

  getDepartmentReport(): Observable<any> {
    return this.http.get(this.apipath + '/reports/departments')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getDepartmentReport', []))
      );
  }

  refresDepartmentReport(refreshData): Observable<any> {
    return this.http.get(this.apipath + '/reports/departments?date_start=' + refreshData.date_start + '&date_end=' + refreshData.date_end + '&department_id=' + refreshData.department_id)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('refresDepartmentReport', []))
      );
  }

  getPresidentReport(): Observable<any> {
    return this.http.get(this.apipath + '/reports/president')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getPresidentReport', []))
      );
  }

  refresPresidentReport(refreshData): Observable<any> {
    return this.http.get(this.apipath + '/reports/president?date_start=' + refreshData.date_start + '&date_end=' + refreshData.date_end)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('refresPresidentReport', []))
      );
  }

  addWorkOrderComment(id, notes): Observable<any> {
    return this.http.post(this.apipath + '/work-orders/' + id + '/comments', notes)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('addWorkOrderComment', []))
      );
  }

  stepTwo(businessName): Observable<any>{
    return this.http.post(this.apipath + '/onboarding/entity-name/store/', businessName)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('stepTwo', []))
      );
  }

  getDepartments(): Observable<any> {
    return this.http.get(this.apipath + '/departments')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getDepartments', []))
      );
  }

  addDepartment(depData): Observable<any>{
    return this.http.post(this.apipath + '/departments/store', depData)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('addDepartment', []))
      );
  }

  deleteDepartment(id): Observable<any> {
    return this.http.delete(this.apipath + '/departments/' + id)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('deleteDepartment', []))
      );
  }

  addBranch(branchData): Observable<any>{
    return this.http.post(this.apipath + '/branches/store', branchData)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('addBranch', []))
      );
  }

  finishOnboardedAt(): Observable<any>{
    return this.http.post(this.apipath + '/onboarding/finish','')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('finishOnboardedAt', []))
      );
  }

  getUsersList(): Observable<any> {
    return this.http.get(this.apipath + '/settings/users')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getUsersList', []))
      );
  }

  getRoles(): Observable<any> {
    return this.http.get(this.apipath + '/settings/roles')
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('getRoles', []))
      );
  }

  saveUser(user): Observable<any>{
    return this.http.post(this.apipath + '/settings/users', user)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('saveUser', []))
      );
  }

  deleteUser(id): Observable<any> {
    return this.http.delete(this.apipath + '/settings/users/' + id)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('deleteUser', []))
      );
  }

  updateUser(id,user): Observable<any>{
    return this.http.put(this.apipath + '/settings/users/' + id, user)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('updateUser', []))
      );
  }

  deleteBranch(id): Observable<any> {
    return this.http.delete(this.apipath + '/branches/' + id)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('deleteBranch', []))
      );
  }

  updateBranch(id, branchData): Observable<any> {
    return this.http.put(this.apipath + '/branches/' + id, branchData)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('updateBranch', []))
      );
  }

  attachPermission(roleData): Observable<any> {
    return this.http.post(this.apipath + '/settings/roles/attach-permission', roleData)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('attachPermission', []))
      );
  }

  detachPermission(roleData): Observable<any> {
    return this.http.post(this.apipath + '/settings/roles/detach-permission', roleData)
      .pipe(
        tap(_ => this.log('response received')),
        catchError(this.handleError('detachPermission', []))
      );
  }

 
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
       console.error(error); 
       this.log(`${operation} failed: ${error.message}`);
       return of(result as T);
    };
  }
 
  private log(message: string) {
    console.log(message);
  }

}