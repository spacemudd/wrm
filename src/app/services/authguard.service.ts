import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';


@Injectable({
  providedIn: 'root'
  })
export class AuthguardService implements CanActivate{

  constructor(
    public authService: AuthenticationService
      ) {}

   canActivate(): boolean {
    return this.authService.isAuthenticated();
    //console.log(this.authService.isAuthenticated());
  }
  
}