import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { ServicesService } from './services.service';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  apipath:string = "https://saremquietrooms.com/api"
  authState = new BehaviorSubject(false);
 
  constructor(
    private router: Router,
    private storage: Storage,
    private platform: Platform,
    public http:HttpClient,
    public svs:ServicesService
  ) {
     this.platform.ready().then(() => {
       this.ifLoggedIn();
    });
  }

  async ifLoggedIn() {
   let response = await this.storage.get('access_token')
      if (response) {
         this.authState.next(true);
      }
  }

  registerUser(user){
      return this.http.post(this.apipath + '/register', user);
    }

    loginUser(email, password){
      const  params = new  HttpParams().set('email', email).set('password', password);
        return this.http.post(this.apipath + '/login', params);
      }
  
    
   logout() {
        this.storage.remove('access_token').then(() => {
        this.svs.userdetail = null;
        this.authState.next(false);
        this.router.navigate(['/login']);  
    });
  }

   exit() {
       this.storage.remove('access_token').then(() => {
        this.authState.next(false);
        this.router.navigate(['/login']);
        navigator['app'].exitApp();
    });
  }

   isAuthenticated() {
    return this.authState.value;
  }

}