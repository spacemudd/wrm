import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  // isLoading = false;
  private currentLoading = null;
  constructor(public loadingCtrl: LoadingController) { }

  // async present() {
  //   this.isLoading = true;
  //   return await this.loadingCtrl.create({
  //     duration: 5000,
  //   }).then(a => {
  //     a.present().then(() => {
  //       console.log('presented');
  //       if (!this.isLoading) {
  //         a.dismiss().then(() => console.log('abort presenting'));
  //       }
  //     });
  //   });
  // }

  async present(message: string = null, duration: number = null){
    if (this.currentLoading != null) {
      this.currentLoading.dismiss();
      console.log('dismiss');
    }
    this.currentLoading = await this.loadingCtrl.create({
      duration: duration,
      message: message
    });
    console.log('loding present');
    return await this.currentLoading.present();
}

  // async dismiss() {
  //   this.isLoading = false;
  //   return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
  // }

  async dismiss() {
    if (this.currentLoading != null) {
      this.currentLoading.dismiss();
      this.currentLoading = null;
      console.log('loading dismiss');
    }
    return;
  }

 

}