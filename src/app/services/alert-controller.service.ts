import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AlertControllerService {

  constructor(public alertController: AlertController, public router: Router) { }

  async presentAlert(title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: ['حسنا']
    });
    await alert.present();
  }

  async presentAlertNewWO(id,title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'حسنا',
          handler: () => {
            alert.dismiss().then(() => {
              this.router.navigate(['workorders/view/' + id]);
            })
          }
        }
      ]
    });
    await alert.present();
  }

  async deleteConfirm(title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'لا',
          handler: () => {
            alert.dismiss(false);
            return false;
          }
        },
        {
          text: 'نعم',
          handler: () => {
            alert.dismiss(true)
            return false;
          }
        }
      ]
    });
   return await alert;
  }

  async successMessage(title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'حسنا',
          handler: () => {
            alert.dismiss();
            return false;
          }
        }
      ]
    });
   return alert;
  }

}
