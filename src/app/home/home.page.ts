import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import {ServicesService } from '../services/services.service';
import {LoadingService } from '../services/loading.service';
import { Router } from '@angular/router';
import { AlertControllerService } from '../services/alert-controller.service';
import { IonRefresher } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
homedata:any;
president_counter:any = {};
executive_counter:any = {};
departments:any = [];
branches:any = [];
icons:any = {};
userdetail: any;

  constructor(public storage: Storage, public svs: ServicesService, public loadingCtrl: LoadingService, public router: Router, public alertCtrl: AlertControllerService) {
  }

  async ngOnInit() {
    this.getUserDetail();
    //this.getHomeData();
    //this.getUserDetail();
  }

   async getHomeData(isrefresher:IonRefresher=null){
    await this.loadingCtrl.present();
    this.svs.getHomeData().subscribe(async (res) => {
      if(isrefresher){
        isrefresher.complete();
      }
      this.homedata = res;
      this.icons = this.homedata.icons;
      if(!this.homedata.president_counter){
        this.president_counter = null;
      }else{
        this.president_counter = this.homedata.president_counter;
      }
      if(!this.homedata.executive_counter)
      {
        this.executive_counter = null;
      }else{
        this.executive_counter = this.homedata.executive_counter;
      }
      if(!this.homedata.departments){
        this.departments = null;
      }else{
        this.departments = this.homedata.departments;
      }
      if(!this.homedata.branches){
        this.branches = null;
      }else{
        this.branches = this.homedata.branches;
      }
    
      this.loadingCtrl.dismiss();
      
      }, err => {
        if(isrefresher){
           isrefresher.complete();
         }
        this.alertCtrl.presentAlert('Error', 'Server error');
        this.loadingCtrl.dismiss();
      })
  }

  addWorkOrder(){
     this.router.navigate(['workorders/new']);
  }
  
  goToWorkList(type,id,status){
    this.router.navigate(['workorders/list/', type, id, status])
  }

  // getUserDetail(){
  //   this.svs.getUserDetail().subscribe(async (res) => {
  //     this.userdetail = res;
  //     console.log('ud' + this.userdetail.department_id);
  //     }, err => {
  //       console.log(err);
  //     })
  // }

  getUserDetail(){
    this.svs.getUserDetail().subscribe(async (res) => {
      this.userdetail = res;
      this.svs.userdetail = this.userdetail;
      console.log('ud' + JSON.stringify(this.userdetail));
      if(this.svs.userdetail.has_active_subscription == 0){
        this.router.navigate(['steps/stepfour']);
      }else{
        this.getHomeData();
      }
      }, err => {
        console.log(err);
      })
  }

  ionRefresh(event) {
    console.log('Pull Event Triggered!');
    this.getHomeData(event.target);
}

departmentList(){
  this.router.navigate(['departments']);
}

}