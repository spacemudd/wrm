import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertControllerService } from 'src/app/services/alert-controller.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ServicesService } from 'src/app/services/services.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  user:any ={};
  public registerForm: FormGroup;
  tc:boolean = false;
  userdata: any;
  userdetail: any;

  constructor(public svs: ServicesService, public formBuilder: FormBuilder, public arouter:ActivatedRoute,  public loadingCtrl: LoadingService, public alertCtrl: AlertControllerService, public router: Router, public navCtrl:NavController, private authService: AuthenticationService, public storage:Storage) {
    this.registerForm = formBuilder.group({
      business_name:['',Validators.compose([Validators.required])],
      name:['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      number:['',Validators.compose([Validators.required])],
      password: ['',Validators.compose([Validators.required])],
      password_confirmation:['',Validators.compose([Validators.required])],
      tc:['']
    });
  }

  ngOnInit() {
    
  }

  async registerUser(){
    if(this.registerForm.valid){
      if(this.user.password !== this.user.password_confirmation){
        this.alertCtrl.presentAlert('Missmatch', 'Password does not match');
      }else{
        if(this.tc === false){
          this.alertCtrl.presentAlert('Accept', 'Please accept the terms of usage');
        }else{
          await this.loadingCtrl.present();
          this.storage.remove('isRemember');
          this.authService.registerUser(this.user).subscribe(async (res) => {
          console.log(res);
          this.authService.loginUser(this.user.email, this.user.password).subscribe(async (res) => {
            console.log(res);
             if(res){
               console.log('success');
               this.userdata = res;                    
               let response = await this.storage.set('access_token', this.userdata.access_token);
                if(response){
                  this.loadingCtrl.dismiss();
                  this.getUserDetail();
                }
             }else{
              this.loadingCtrl.dismiss();
               this.alertCtrl.presentAlert('Alert', 'Signup error');
             }     
          })
        }, err => {
          console.log(err);
          this.loadingCtrl.dismiss();
        })
      }
      }
    }else{
      this.alertCtrl.presentAlert('Required', 'All Fields are required');
    }
  }

  async getUserDetail(){
    await this.loadingCtrl.present();
      this.svs.getUserDetail().subscribe(async (res) => {
      if(res){
        this.userdetail = res;
        this.svs.userdetail = this.userdetail;
        this.loadingCtrl.dismiss();
        if(!this.userdetail.onboarded_at){
          this.authService.authState.next(true);
          this.router.navigate(['/steps/stepone/', this.user.business_name]);
        }else if(this.userdetail.onboarded_at != null && this.userdetail.has_active_subscription ==0){
          this.authService.authState.next(true);
          this.router.navigate(['/steps/stepfour/']);
        }else{
          this.router.navigate(['/steps/stepone/', this.user.business_name]);
        }
      }else{
        this.loadingCtrl.dismiss();
        console.log('Error');
      }
      
      console.log('ud' + JSON.stringify(this.userdetail));
      }, err => {
        console.log(err);
      })
  }

}
