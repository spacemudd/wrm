import { NgModule, ErrorHandler } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy, Platform, DomController  } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'
import { IonicStorageModule } from '@ionic/storage';
import { HttpConfigInterceptor } from './interceptors/httpConfig.interceptor';

//import { IOSFilePicker } from '@ionic-native/file-picker';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import  { Base64 } from '@ionic-native/base64/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { AuthguardService } from './services/authguard.service';
import { AuthenticationService } from './services/authentication.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { ComponentsModule } from './components/components.module';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, IonicStorageModule.forRoot(), NgxIonicImageViewerModule,ComponentsModule],
  providers: [
    StatusBar,
    SplashScreen,
    AuthguardService,
    AuthenticationService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    },
    // { provide: ErrorHandler, useClass: HttpConfigInterceptor },
    //IOSFilePicker,
    FileChooser,
    Camera,
    Base64,
    FilePath,
    ImagePicker,
    File,
    Deeplinks
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
