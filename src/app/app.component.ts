import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { ServicesService } from './services/services.service';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  navigate : any;
  userdetail:any;
  has_active_subscription:number= 0;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private router: Router,
    private authenticationService: AuthenticationService,
    private svs: ServicesService,
    private deeplinks: Deeplinks
  ) {
    //this.sideMenu();
    this.initializeApp();
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if(this.platform.is('cordova'))
      this.initDeep();

      this.authenticationService.authState.subscribe(state => {
        if (state) {
          this.getUserDetail();
          this.router.navigate(['/home']);
          // if(this.svs.userdetail.has_active_subscription == 0){
          //   this.router.navigate(['/steps/stepfour']);
          // }else{
          //   this.router.navigate(['/home']);
          // }
        } else {
          this.router.navigate(['/login']);
        }
      });

    });
  }

  sideMenu()
  {
    this.navigate =
    [
      {
        title : "الرئيسية",
        url   : "/home",
        icon  : "home"
      },
      {
        title : "تقارير",
        url   : "/reports",
        icon  : "document"
      },
      // {
      //   title : "الإعدادت",
      //   url   : "/settings",
      //   icon  : "settings"
      // }     
    ]
  }

  newWorkOrder(){
   this.router.navigate(['workorders/new']);
  }

  homePage(){
    this.router.navigate(['home']);
  }

  reportsPage(){
    this.router.navigate(['reports']);
  }

  settingsPage(){
    this.router.navigate(['settings']);
  }

  logout(){
    this.authenticationService.logout();
  }

  exit(){
    this.authenticationService.exit();
  }

  getUserDetail(){
    this.svs.getUserDetail().subscribe(async (res) => {
      this.userdetail = res;
      this.svs.userdetail = this.userdetail;
      console.log('ud' + JSON.stringify(this.userdetail));
      this.has_active_subscription = this.svs.userdetail.has_active_subscription;
      // if(this.svs.userdetail.has_active_subscription == 0){
      //   console.log('step4');
      //   this.router.navigate(['/steps/stepfour']);
      // }else{
      //   this.router.navigate(['/home']);
      // }
      }, err => {
        console.log(err);
      })
  }

  initDeep(){
    this.deeplinks.route({
      '/resetpassword/:token': 'resetpassword',     
    }).subscribe(match => {
      // match.$route - the route we matched, which is the matched entry from the arguments to route()
      // match.$args - the args passed in the link
      // match.$link - the full link data
      console.log('Successfully matched route', match);
      if(match.$args.token && match.$args.email)
      this.router.navigate([match.$route,match.$args])
    }, nomatch => {
      // nomatch.$link - the full link data
      console.error('Got a deeplink that didn\'t match', nomatch);
    });
  }

}


