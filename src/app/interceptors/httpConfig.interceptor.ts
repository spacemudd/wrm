import {Injectable} from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError, from } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


const TOKEN_KEY = 'access_token';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

    //protected url   = 'https://saremquietrooms.com/api';
    protected debug = true;

    constructor(private alertController: AlertController, private storage: Storage) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log(request);
        // YOU CAN ALSO DO THIS
        // const token = this.authenticationService.getToke()

        return from(this.storage.get(TOKEN_KEY))
            .pipe(
                switchMap(token => {
                    if (token && this.isTokenTobeAdd(request.url)) {
                        //alert(token);
                        request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token).set('Accept', 'application/json') });
                    }
                  
                    return next.handle(request).pipe(
                        map((event: HttpEvent<any>) => {
                            if (event instanceof HttpResponse) {
                                console.log(event);
                            }
                            return event;
                        }),
                        catchError((error: HttpErrorResponse) => {                            
                            return throwError(error);
                        })
                    );
                })
            );


    }
    isTokenTobeAdd(url) {
        const re = ['login','upload.php','signup'];
        let checkurl = url.split('/');
        if (re.indexOf(checkurl[checkurl.length - 1]) == -1)
            return true;
        else
            return false;
    }

   
}