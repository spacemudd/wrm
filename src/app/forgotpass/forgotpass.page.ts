import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ServicesService } from '../services/services.service';
import { LoadingService } from '../services/loading.service';
import { AlertControllerService } from '../services/alert-controller.service';

@Component({
  selector: 'app-forgotpass',
  templateUrl: './forgotpass.page.html',
  styleUrls: ['./forgotpass.page.scss'],
})
export class ForgotpassPage implements OnInit {
user = {email:''};
  constructor(public navCtrl: NavController, private apiService: ServicesService, private loadingService: LoadingService, private alertService: AlertControllerService) { }

  ngOnInit() {
  }

 async onForgotPass(){
    console.log(this.user.email)
    if(this.user.email && this.user.email != ''){
      await this.loadingService.present();
      this.apiService.forgotPass(this.user.email).subscribe(async(res:any) => {
        console.log(res);
        this.loadingService.dismiss();
        if(res.success){
          let success = await this.alertService.successMessage('تم بنجاح','تم إرسال تفاصيل استعادة كلمة المرور الخاصة بك إلى بريدك الإلكتروني!')
          await success.present();
          await success.onDidDismiss();
          this.navCtrl.pop();
        }else{          
          this.alertService.presentAlert('يوجد خطأ',res.errors.email);
        }
        
      },err => {
        this.loadingService.dismiss();
        this.alertService.presentAlert('يوجد خطأ','يوجد خطأ، الرجاء محاولة مرة أخرى'); //There is some error, please try again later
      })
    }else{
      this.alertService.presentAlert('يوجد خطأ','الرجاء إدخال بريدك الإكتروني'); //Please enter email
    }
  }

}
