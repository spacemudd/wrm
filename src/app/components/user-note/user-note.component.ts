import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams  } from '@ionic/angular';
import {ServicesService } from '../../services/services.service';
import {LoadingService } from '../../services/loading.service';
import { AlertControllerService } from '../../services/alert-controller.service';

@Component({
  selector: 'app-user-note',
  templateUrl: './user-note.component.html',
  styleUrls: ['./user-note.component.scss'],
})
export class UserNoteComponent implements OnInit {
id: any;
value: any;
note:any;
  constructor(private modalController: ModalController, public loadingCtrl: LoadingService, public svs: ServicesService, public alertCtrl: AlertControllerService, public navParams: NavParams) { }

  ngOnInit() {
   this.id = this.navParams.data.id;
   this.value = this.navParams.data.value;
  }

  async closeModal(){
    await this.modalController.dismiss();
  }

  async completeOrEscalateWork(){
    if(this.value === 'complete'){
      this.loadingCtrl.present();
      this.svs.completeWork(this.id, this.note).subscribe(async (res) => {
        console.log(res);
        if(res){
          this.loadingCtrl.dismiss();
          this.alertCtrl.presentAlert('تم النجاح', 'Work conpleted successfully');
          await this.modalController.dismiss();
        }
        }, err => {
          console.log(err);
          this.loadingCtrl.dismiss();
        })
    }else if(this.value == 'escalate'){
      this.loadingCtrl.present();
      this.svs.escalateWork(this.id, this.note).subscribe(async (res) => {
        console.log(res);
        if(res){
          this.loadingCtrl.dismiss();
          this.alertCtrl.presentAlert('تم النجاح', 'Work escalated successfully');
          await this.modalController.dismiss();
        }
        }, err => {
          console.log(err);
          this.loadingCtrl.dismiss();
        })
    }else if(this.value == 'respond'){
      if(!this.note){
        this.alertCtrl.presentAlert('Required', 'Please add your notes');
      }else{
        this.loadingCtrl.present();
        this.svs.respondWorkOrder(this.id, this.note).subscribe(async (res) => {
          console.log(res);
          if(res){
            this.loadingCtrl.dismiss();
            this.alertCtrl.presentAlert('ok', 'Resond successfully');
            await this.modalController.dismiss();
          }
          }, err => {
            console.log(err);
            this.loadingCtrl.dismiss();
          })
      }
    }else {
      if(!this.note){
        this.alertCtrl.presentAlert('Required', 'Please add your notes');
      }else{
        this.loadingCtrl.present();
        this.svs.returnWorkOrder(this.id, this.note).subscribe(async (res) => {
          console.log(res);
          if(res){
            this.loadingCtrl.dismiss();
            this.alertCtrl.presentAlert('ok', 'Returned successfully');
            await this.modalController.dismiss();
          }
          }, err => {
            console.log(err);
            this.loadingCtrl.dismiss();
          })
      }
    }
  }

  // async completeWork(){
  //   this.loadingCtrl.present();
  //   this.svs.completeWork(this.id, this.note).subscribe(async (res) => {
  //     console.log(res);
  //     if(res){
  //       this.loadingCtrl.dismiss();
  //       this.alertCtrl.presentAlert('Success', 'Work conpleted successfully');
  //       await this.modalController.dismiss();
  //     }
  //     }, err => {
  //       console.log(err);
  //       this.loadingCtrl.dismiss();
  //     })
  // }

  // async escalateWork(){
  //   this.loadingCtrl.present();
  //   this.svs.escalateWork(this.id, this.note).subscribe(async (res) => {
  //     console.log(res);
  //     if(res){
  //       this.loadingCtrl.dismiss();
  //       this.alertCtrl.presentAlert('Success', 'Work escalated successfully');
  //       await this.modalController.dismiss();
  //     }
  //     }, err => {
  //       console.log(err);
  //       this.loadingCtrl.dismiss();
  //     })
  // }

}