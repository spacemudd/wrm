import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserNoteComponent } from './user-note/user-note.component';
import { IonicModule } from '@ionic/angular';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [UserNoteComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  exports: [UserNoteComponent],
  entryComponents: [UserNoteComponent]
})
export class ComponentsModule { }
