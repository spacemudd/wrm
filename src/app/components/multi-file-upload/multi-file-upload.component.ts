import { Component, OnInit, Sanitizer } from '@angular/core';
//import { FileUploader, FileLikeObject } from 'ng2-file-upload';
import { NavController, Platform, ActionSheetController,ToastController } from "@ionic/angular";
//import { IOSFilePicker } from "@ionic-native/file-picker";
import { FileChooser } from "@ionic-native/file-chooser/ngx";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { Base64 } from "@ionic-native/base64/ngx";
import { FilePath } from "@ionic-native/file-path/ngx";
import { AlertControllerService } from '../../services/alert-controller.service';
import {ServicesService } from '../../services/services.service';
import {LoadingService } from '../../services/loading.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-multi-file-upload',
  templateUrl: './multi-file-upload.component.html',
  styleUrls: ['./multi-file-upload.component.scss'],
})
export class MultiFileUploadComponent implements OnInit {
  camera_image: SafeResourceUrl;
  workorder:any = {};
  departments:any;
  priorities:any;
  public workOrderForm: FormGroup;
  userdetail:any;

  fileArray: Array<{ displayFile: any; base64File: string }> = [];
  // imageArr: Array<{ files: any; base64Img: string }> = [];
  imageArr: Array<{ base64Img: string }> = [];
  // public uploader: FileUploader = new FileUploader({});
  // public hasBaseDropZoneOver: boolean = false;
  imageArrayBase64:string='';
  //imgAr : any = [];
  fileNames:any=[];
  branches:any;
  constructor(
    public navCtrl: NavController,
    public base64: Base64,
    public camera: Camera,
    public fileChooser: FileChooser,
    public plt: Platform,
    //public filePicker: IOSFilePicker,
    public actionSheetCtrl: ActionSheetController,
    public filePath: FilePath,
    public toastCtrl: ToastController,
    public imagePicker: ImagePicker,
    public file: File,
    public sanitizer: DomSanitizer,
    public svs: ServicesService, public formBuilder: FormBuilder, public loadingCtrl: LoadingService, public alertCtrl: AlertControllerService, public router: Router
  ) 
  {
    this.workOrderForm = formBuilder.group({
      department: ['', Validators.compose([Validators.required])],
      priority: ['',Validators.compose([Validators.required])],
      description: ['',Validators.compose([Validators.required, Validators.maxLength(256)])],
      created_by_branch_id:[{disabled: true, value:""}]
    });
  }

  // getFiles(): FileLikeObject[] {
  //   return this.uploader.queue.map((fileItem) => {
  //     return fileItem.file;
  //   });
  // }

  // fileOverBase(ev): void {
  //   this.hasBaseDropZoneOver = ev;
  // }

  // reorderFiles(reorderEvent: CustomEvent): void {
  //   let element = this.uploader.queue.splice(reorderEvent.detail.from, 1)[0];
  //   //alert(element);
  //   this.uploader.queue.splice(reorderEvent.detail.to, 0, element);
  // }

  ngOnInit() {
    console.log(this.svs.userdetail);
    this.getDepartment();

    this.getPriority();
    // this.getUserDetail();
  }

  getDepartment(){
    this.loadingCtrl.present();
    this.svs.getDepartment().subscribe(async (res) => {
      this.departments = res;
      if(this.svs.userdetail && !this.svs.userdetail.branch_id){
        this.branches =  await this.svs.getBranches().toPromise();
        this.workOrderForm.get('created_by_branch_id').enable();
      }
     // this.getPriority();
     // this.getUserDetail();
      console.log(res);
      this.loadingCtrl.dismiss();
      }, err => {
        console.log(err);
        this.loadingCtrl.dismiss();
      })
  }

  getPriority(){
    this.svs.getPriority().subscribe(async (res) => {
      this.priorities = res;
      console.log(res);
      }, err => {
        console.log(err);
      })
  }
  
  // getUserDetail(){
  //   this.svs.getUserDetail().subscribe(async (res) => {
  //     this.userdetail = res;
  //     }, err => {
  //       console.log(err);
  //     })
  // }

  async presentActionSheet() {
    let actionSheet = await this.actionSheetCtrl.create({
      header: "Pick image to upload",
      buttons: [
        {
          text: "From Gallery",

          handler: () => {
            this.openGallery();
          }
        },
        {
          text: "From Camera",
          handler: () => {
            this.openCamera();
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });

    actionSheet.present();
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      cameraDirection: 1,
      destinationType: this.camera.DestinationType.FILE_URI
    };
    this.camera
      .getPicture(options)
      .then(imageData => {
        //console.log("IMAGE DATA IS", imageData);
        this.presentToast("Image chosen successfully");
        this.convertToBase64(imageData, true);
      })
      .catch(e => {
        console.log("Error while picking from camera", e);
      });
  }

  // openGallery() {
  //   var options = {
  //     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //   };
  //   this.camera
  //     .getPicture(options)
  //     .then(imageData => {
  //       console.log("IMAGE DATA IS", imageData);
  //       this.presentToast("Image chosen successfully");
  //       this.convertToBase64(imageData, true);
  //     })
  //     .catch(e => {
  //       console.log("Error while picking from gallery", e);
  //     });
  // }

  openGallery(){
   var options:ImagePickerOptions = {
    maximumImagesCount: 5
   }
   this.imagePicker.getPictures(options).then((result) =>{
     for(var interval = 0;interval<result.length;interval++)
     {
       let filename = result[interval].substring(result[interval].lastIndexOf('/') + 1);
       let path = result[interval].substring(0, result[interval].lastIndexOf('/') + 1);
       this.file.readAsDataURL(path,filename).then((base64string : string) => {
         console.log(base64string.split(',').pop());
         this.fileNames.push({'name':filename, 'image':base64string});
         console.log(base64string);
         this.imageArr.push({
         base64Img:base64string.split(",").pop()
         });
       })
     }
   })
   }
  
  chooseFile() {
    if (this.plt.is("ios")) {
      //this.chooseFileForIos();
    } else {
      this.chooseFileForAndroid();
    }
  }

  // chooseFileForIos() {
  //   this.filePicker
  //     .pickFile()
  //     .then(uri => {
  //       console.log(uri);
  //       this.presentToast("File chosen successfully");
  //       this.convertToBase64(uri,false)
  //     })
  //     .catch(err => console.log("Error", err));
  // }

  chooseFileForAndroid() {
    this.fileChooser
      .open()
      .then(uri => {
        console.log(uri);
        this.presentToast("File chosen successfully");
        this.convertToBase64(uri,false)
      })
      .catch(e => {
        console.log(e);
      });
  }

  convertToBase64(imageUrl, isImage) {
    this.filePath
      .resolveNativePath(imageUrl)
      .then(filePath => {
        console.log(filePath);
        this.base64.encodeFile(filePath).then(
          
          (base64File: string) => {
            console.log("BASE 64 IS", filePath.split(".").pop());
            if (isImage == false) {
              this.fileArray.push({
                displayFile: filePath.split("/").pop(),
                base64File: base64File.split(",").pop() 
              });
            } else {
              //this.fileNames = filePath.substring(filePath.lastIndexOf('/') + 1);
              //this.fileNames = this.fileNames.concat(filePath.substring(filePath.lastIndexOf('/') + 1) + '<br/>');
              this.camera_image = this.sanitizer.bypassSecurityTrustResourceUrl(base64File);
              this.fileNames.push({ 'name':filePath.substring(filePath.lastIndexOf('/') + 1), 'image': this.camera_image });
              this.imageArr.push({
                 //files: filePath,
                // base64Img: base64File.split(",").pop() //same comment for image follows here.
                base64Img:base64File.split(",").pop()
              });
              //this.imageArr.push(base64File.split(",").pop());
            }
            console.log("LENGTH OF BASE64ARR", this.imageArr.length);
            console.log("IMAGE ARRAY", this.imageArr);
            console.log(this.fileNames);
          },
          err => {
            console.log(err);
          }
        );
      })
      .catch(err => console.log(err));
  }

    async presentToast(message) {
    let toast =  await this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
  
    // toast.onDidDismiss(() => {
    //   //console.log('Dismissed toast');
    // });
  
    toast.present();
  }

  addWorkOrder(){
    if(this.workOrderForm.valid){
      for(var j=0;j<this.imageArr.length;j++){
        if(this.imageArrayBase64 == ""){
          this.imageArrayBase64 = this.imageArr[j].base64Img;
          //this.imgAr.push(this.imageArr[j].base64Img);
        }else{
          this.imageArrayBase64 = this.imageArrayBase64  +"," + this.imageArr[j].base64Img;
          //this.imgAr.push(this.imageArr[j].base64Img);
        }
      }
      //debugger;
      console.log('imgarr');
      console.log(this.imageArrayBase64);
      //var imgarr:any = this.imageArr;
      var formData = new FormData();
      
      formData.append("files", this.imageArrayBase64);
      formData.append('priority_id', this.workorder.priority);
      formData.append('description', this.workorder.description);
      formData.append('department_id', this.workorder.department);
      if(this.svs.userdetail.branch_id == null){
      formData.append('created_by_branch_id', this.workorder.branch);
      }
      // else{
      // formData.append('created_by_branch_id', this.userdetail.branch_id);
      // }
      console.log(formData);
      
      //console.log(imgarr);
      this.loadingCtrl.present();
       this.svs.addWorkOrder(formData).subscribe(async (res:any) => {
         if(res){
          this.loadingCtrl.dismiss();
          this.alertCtrl.presentAlertNewWO(res.id,'تم', 'تم إنهاء الطلب بنجاع');
         }
         }, err => {
           console.log(err);
           this.loadingCtrl.dismiss();
         })
    }else{
      this.alertCtrl.presentAlert('Required', 'All Fields are required');
    }
  }

  cancel(){
    this.router.navigate(['/home']);
  }


}