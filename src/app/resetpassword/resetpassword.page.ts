import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertControllerService } from '../services/alert-controller.service';
import { LoadingService } from '../services/loading.service';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.page.html',
  styleUrls: ['./resetpassword.page.scss'],
})
export class ResetpasswordPage implements OnInit {
user={email:'',password:'',password_confirmation:''};
token:string;
  constructor(
            private route:ActivatedRoute, 
            private router: Router, 
            private alertService: AlertControllerService,
            private loadingService: LoadingService,
            private apiService: ServicesService
    ) {    
    this.user.email = this.route.snapshot.paramMap.get('email');
    this.token = this.route.snapshot.paramMap.get('token');
    console.log(this.user, this.token)
   }

  ngOnInit() {
  }

  gotologin(){
    this.router.navigate(['/login']);
  }

 async resetPassword(){
    if(!this.user.password || !this.user.password_confirmation || !this.user.email){
      this.alertService.presentAlert('يوجد خطأ',"كل الحقول مطلوبة") //All Fields are required
      return;
    }
    if(this.user.password_confirmation != this.user.password){
      this.alertService.presentAlert('يوجد خطأ',"كلمة السر وتأكيد كلمة السر غير مطابقين"); //Passsword and confirm password doesn't match
      return;
    }
   await this.loadingService.present();
   let data = {
     token:this.token,
     email: this.user.email,
     password: this.user.password,
     password_confirmation: this.user.password_confirmation
   }
   this.apiService.resetPass(data).subscribe(async(res:any) => {
     console.log(res);
     this.loadingService.dismiss();
     if(res.success){
      let success = await this.alertService.successMessage('تم بنجاح',"تم تغير كلمة السر بنجاح"); //password reset successfully
      await success.present();
      await success.onDidDismiss();
      this.router.navigate(['/login'])
     }else{
       this.alertService.presentAlert('يوجد خطأ',res.errors.email || res.errors.password || res.errors.password_confirmation || res.errors.token || 'يوجد خطأ، الرجاء محاولة مرة أخرى')
     }
     
   },err => {
     console.log(err);
     this.loadingService.dismiss();
     this.alertService.presentAlert('يوجد خطأ','يوجد خطأ، الرجاء محاولة مرة أخرى')
   })
  }

}

