import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthguardService } from './services/authguard.service';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
    canActivate: [AuthguardService]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path:'workorders',
    loadChildren:() => import('./workorders/workorders.module').then(m => m.WorkordersModule),
    canActivate: [AuthguardService]
  },
  {
    path:'reports',
    loadChildren:() => import('./reports/reports.module').then(m => m.ReportsModule),
    canActivate: [AuthguardService]
  },
  {
    path:'settings',
    loadChildren:() => import('./settings/settings.module').then(m => m.SettingsModule),
    canActivate: [AuthguardService]
  },
  {
    path:'departments',
    loadChildren:() => import('./departments/departments.module').then(m => m.DepartmentsModule),
    canActivate: [AuthguardService]
  },
  {
    path: 'signup',
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'steps',
    loadChildren: () => import('./steps/steps.module').then( m => m.StepsModule)
  },  {
    path: 'forgotpass',
    loadChildren: () => import('./forgotpass/forgotpass.module').then( m => m.ForgotpassPageModule)
  },
  {
    path: 'resetpassword',
    loadChildren: () => import('./resetpassword/resetpassword.module').then( m => m.ResetpasswordPageModule)
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }